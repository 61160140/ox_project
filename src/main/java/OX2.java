import java.util.Scanner;
public class OX2 {
    public static void showWelcome(){
        System.out.println("Welcome to OX Game");
    }
    public static char[][] createTable(){
        char[][] table = {{'-','-','-'},
                            {'-','-','-'},
                            {'-','-','-'}};
        return table;
    }
    public static void showTable(char[][] table){
        for(int i=0;i<table.length;i++){
            for(int j=0;j<table[i].length;j++){
                System.out.print(table[i][j]+" ");
            }
            System.out.println("");
        }
    }
    public static void showPlayer(char turn){
        System.out.println(turn+" Turn");
    }
    public static void showInput(){
        System.out.print("Please input row, col :");        
    }    
    public static void input(int row,int col,char[][] table,char turn) throws Exception{
        if(table[row-1][col-1]=='-'){
            table[row-1][col-1] = turn;
        }else{
            throw new Exception();
        }
    }
    public static void showWrongInput(){
        System.out.println("Wrong Position please input again!");
    }
    public static void showWin(char turn){
        System.out.println("Player "+turn+" Win!!");
    }
    public static void showDraw(){
        System.out.println("Draw!!");
    }
    public static boolean checkDraw(int count){
        if (count==9){
           showDraw();
           return true;
        }
        return false;
    }
    
    public static boolean checkWin(char turn,char[][] table){
        if (checkRow(turn,table)||checkCol(turn,table)||checkX1(turn,table)||checkX2(turn,table)){
            showWin(turn);
            return true;
        }
        return false;
    }
    public static boolean checkRow(char turn,char[][] table){
        for(int i=0;i<table.length;i++){
            if(table[i][0]==table[i][1] && table[i][0]==table[i][2] && table[i][0]==turn){
                return true;
            }
        }
        return false;
    }
    public static boolean checkCol(char turn,char[][] table){
        for(int i=0;i<table.length;i++){
            if(table[0][i]==table[1][i] && table[0][i]==table[2][i] && table[0][i]==turn){
                return true;
            }
        }
        return false;
    }
    public static boolean checkX1(char turn,char[][] table){
        if(table[0][0]==table[1][1] && table[0][0]==table[2][2] && table[0][0]==turn){
            return true;
        }
        return false;
    }
    public static boolean checkX2(char turn,char[][] table){
        if(table[0][2]==table[1][1] && table[0][2]==table[2][0] && table[0][2]==turn){
            return true;
        }
        return false;
    }
    public static char switchPlayer(char turn){
        if(turn=='O'){
            return 'X';
        }else{
            return 'O';
        }
    }
    public static void showBye(){
        System.out.println("Bye bye!!");
    }
    
    public static void main(String[] args){
        Scanner kb  = new Scanner(System.in);
        int count = 0;
        char[][] oxtable = createTable();
        char turn = 'O';
        showWelcome();
        showTable(oxtable);
        while(true){
        try{
            showPlayer(turn);
            showInput();
            input(kb.nextInt(),kb.nextInt(),oxtable,turn);
        }catch(Exception e){
            showWrongInput();
            continue;
        }
        showTable(oxtable);
            count++;
            if(checkWin(turn,oxtable)|| checkDraw(count)==true){
                showBye();
                break;
            }
            turn = switchPlayer(turn);
        }
    }
    
}
