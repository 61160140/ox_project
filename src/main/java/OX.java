import java.util.Scanner;
public class OX {
    public static void main (String[] args){
        Scanner kb = new Scanner(System.in);
        char[][] oxtable = {{'-','-','-'},
                            {'-','-','-'},
                            {'-','-','-'}};
        int row = 0;
        int col = 0;
        char turn = 'O';
        
        System.out.println("Welcome to OX Game");       
        for(int i=0;i<oxtable.length;i++){
            for(int j=0;j<oxtable.length;j++){
                System.out.print(oxtable[i][j]+" ");
            }
            System.out.println("");
        }
        //Game Start :3
        while(true){
            if(turn=='O'){
                System.out.println("Turn O");
            }else if (turn=='X'){
                System.out.println("Turn X");
            }
            System.out.print("Please input row, col :");
            row = kb.nextInt();
            col = kb.nextInt();
            
            if(oxtable[row-1][col-1]=='-'){
                oxtable[row-1][col-1] = turn;
            }else{
                System.out.println("Wrong Position input again!");
                continue;
            }
            for(int i=0;i<oxtable.length;i++){
                for(int j=0;j<oxtable.length;j++){
                    System.out.print(oxtable[i][j]+" ");
                }
            System.out.println("");
            }
            //condition
            if(oxtable[0][0]==oxtable[0][1] && oxtable[0][0]==oxtable[0][2] && oxtable[0][0]!='-'){
                System.out.println(oxtable[0][0]+" Win !!");
                break;
            }else if(oxtable[1][0]==oxtable[1][1] && oxtable[1][0]==oxtable[1][2] && oxtable[1][0]!='-'){
                System.out.println(oxtable[1][0]+" Win !!");
                break;
            }else if(oxtable[2][0]==oxtable[2][1] && oxtable[2][0]==oxtable[2][2] && oxtable[2][0]!='-'){
                System.out.println(oxtable[2][0]+" Win !!");
                break;
            }else if(oxtable[0][0]==oxtable[1][0] && oxtable[0][0]==oxtable[2][0] && oxtable[0][0]!='-'){
                System.out.println(oxtable[0][0]+" Win !!");
                break;
            }else if(oxtable[0][1]==oxtable[1][1] && oxtable[0][1]==oxtable[2][1] && oxtable[0][1]!='-'){
                System.out.println(oxtable[0][1]+" Win !!");
                break;
            }else if(oxtable[0][2]==oxtable[1][2] && oxtable[0][2]==oxtable[2][2] && oxtable[0][2]!='-'){
                System.out.println(oxtable[0][2]+" Win !!");
                break;
            }else if(oxtable[0][0]==oxtable[1][1] && oxtable[0][0]==oxtable[2][2] && oxtable[0][0]!='-'){
                System.out.println(oxtable[0][0]+" Win !!");
                break;
            }else if(oxtable[0][2]==oxtable[1][1] && oxtable[0][2]==oxtable[2][0] && oxtable[0][2]!='-'){
                System.out.println(oxtable[0][2]+" Win !!");
                break;
            }else{
                boolean flag = true;
                for(int i=0;i<oxtable.length;i++){
                    for(int j=0;j<oxtable.length;j++){
                        if(oxtable[i][j]=='-'){
                        flag = false;
                        }
                    }
                }
                if(flag == true){
                System.out.println("Draw !!");
                break;
                }
            }
            
            if(turn=='O'){
                turn = 'X';
            }else if (turn=='X'){
                turn = 'O';
            }
           
        }
        
    }
    
}
